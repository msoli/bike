from bike import models, fields


SEXO = (
	('M', 'MASCULINO'),
	('F', 'FEMININO'),
	('V', 'OUTRO'),
)

ESTADO_CIVIL = (
	('C', 'CASADO'),
	('S', 'SOLTEIRO'),
	('V', 'VIUVO'),
	('D', 'DIVORCIADO'),
	('A', 'AMANCEBADO'),
)

UFS = ['SP', 'RN', 'BA']


class Telefone(models.Model):
	ddd = fields.String(size=2)
	numero = fields.String()
	

class Endereco(models.Model):
	logradouro = fields.String()
	numero = fields.String()
	complemento = fields.String()
	bairro = fields.String()
	cidade = fields.String()
	uf = fields.String(size=2, options=UFS)
	

class Pessoa(models.Model):
	nome = fields.String(required=True)
	cpf = fields.String(required=True)
	nascimento = fields.Date(required=True)
	sexo = fields.Char(default='M', options=SEXO)
	estado_civil = fields.Char(options=ESTADO_CIVIL)
	endereco = fields.Nested(Endereco)
	telefones = fields.List(Telefone)

	class Meta:
		pass
	
	def __str__(self):
		return self.nome
		
		
data = {
	'nome': 'Manasses Lima',
	'cpf': '03518446436',
	'nascimento': '1977-12-26',
	'sexo': 'M',
	'estado_civil': 'T',
	'endereco': {
		'logradouro': 'Rua das Plameiras',
		'numero': '217',
		'complemento': 'apto. 46B',
		'bairro': 'Vila Augusta',
		'cidade': 'Guarulhos',
		'uf': 'SP',
		'cep': '07022000'
	},
	'telefones': [
		{
			'ddd': '11',
			'numero': '9843364250'
		},
		{
			'ddd': '11',
			'numero': '967874532'
		},
		{
			'ddd': '84',
			'numero': '996435656'
		}
	]
}

try:
	p1 = Pessoa.load(data)
	print(p1)
except Exception as err:
	print(err.args[0])
	
	
	

